resource "aws_lambda_function" "example" {

  filename      = "lambda_function.zip"
  function_name = "lambda_efs_guardrail"
  role          = "arn:aws:iam::991836097896:role/service-role/efs-check-guardrails-role-bcl7uwr2"
  runtime = "python3.9"
  handler = "lambda_function.lambda_handler"
  timeout = 180
  kms_key_arn = "arn:aws:kms:us-east-1:991836097896:key/b7311e31-2ec9-4338-a826-80618c26ab78"

  vpc_config {
    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
    subnet_ids         = ["subnet-0b2f518dd8b62cd1f", "subnet-01eba6fce0c740e2f", "subnet-0026ed7164b065afd"]
    security_group_ids = ["sg-0afd5631024915da6"]
  }

  tags = {
      tech-team-email = "robson@itau-unibanco.com"
  }

  environment {
    variables = {
      foo = "bar"
    }
  }
}
